package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import Model.DataObject.DataContract.CharacterContract;
import Model.DataObject.Character;

/**
 * Created by Jeff on 23/03/2018.
 */

public class DataSource {
    private SQLiteDatabase mDatabase;
    private final DBHelper mDBHelper;

    private final String[] CHARACTERS_ALL_COLUMNS = {
                    CharacterContract.CharacterEntry.COLUMN_NAME_ID,
                    CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_NAME,
                    CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS,
                    CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_RACE,
                    CharacterContract.CharacterEntry.COLUMN_NAME_LEVEL,
                    CharacterContract.CharacterEntry.COLUMN_NAME_STR,
                    CharacterContract.CharacterEntry.COLUMN_NAME_DEX,
                    CharacterContract.CharacterEntry.COLUMN_NAME_CON,
                    CharacterContract.CharacterEntry.COLUMN_NAME_INT,
                    CharacterContract.CharacterEntry.COLUMN_NAME_WIS,
                    CharacterContract.CharacterEntry.COLUMN_NAME_CHA,
                    CharacterContract.CharacterEntry.COLUMN_NAME_ACROBATICS,
                    CharacterContract.CharacterEntry.COLUMN_NAME_ANIMAL_HANDLING,
                    CharacterContract.CharacterEntry.COLUMN_NAME_ARCANA,
                    CharacterContract.CharacterEntry.COLUMN_NAME_ATHLETICS,
                    CharacterContract.CharacterEntry.COLUMN_NAME_DECEPTION,
                    CharacterContract.CharacterEntry.COLUMN_NAME_HISTORY,
                    CharacterContract.CharacterEntry.COLUMN_NAME_INSIGHT,
                    CharacterContract.CharacterEntry.COLUMN_NAME_INTIMIDATION,
                    CharacterContract.CharacterEntry.COLUMN_NAME_INVESTIGATION,
                    CharacterContract.CharacterEntry.COLUMN_NAME_MEDICINE,
                    CharacterContract.CharacterEntry.COLUMN_NAME_NATURE,
                    CharacterContract.CharacterEntry.COLUMN_NAME_PERCEPTION,
                    CharacterContract.CharacterEntry.COLUMN_NAME_PERFORMANCE,
                    CharacterContract.CharacterEntry.COLUMN_NAME_PERSUASION,
                    CharacterContract.CharacterEntry.COLUMN_NAME_RELIGION,
                    CharacterContract.CharacterEntry.COLUMN_NAME_SLEIGHT_OF_HAND,
                    CharacterContract.CharacterEntry.COLUMN_NAME_STEALTH,
                    CharacterContract.CharacterEntry.COLUMN_NAME_SURVIVAL
    };

    private final String[] CHARACTERS_INFO_COLUMNS = {
            CharacterContract.CharacterEntry.COLUMN_NAME_ID,
            CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_NAME,
            CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS,
            CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_RACE,
            CharacterContract.CharacterEntry.COLUMN_NAME_LEVEL
    };

    public void open() {
        mDatabase = mDBHelper.getWritableDatabase();
    }
    public void close() {
        mDBHelper.close();
    }

    public DataSource(Context context) {
        mDBHelper =  new DBHelper(context);
    }

    /**
     * Save an object within the mDatabase.
     * @param character the object to be saved.
     */
    public void save(Character character) {
        ContentValues values = new ContentValues();
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_NAME, character.getName());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS, character.getCharacterClass());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_RACE, character.getRace());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_LEVEL, character.getLevel());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_STR, character.getStrScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_DEX, character.getDexScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CON, character.getConScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INT, character.getIntScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_WIS, character.getWisScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHA, character.getChaScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ACROBATICS, character.getSkillProf(Character.Skill.Acrobatics).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ANIMAL_HANDLING, character.getSkillProf(Character.Skill.AnimalHandling).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ARCANA, character.getSkillProf(Character.Skill.Arcana).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ATHLETICS, character.getSkillProf(Character.Skill.Athletics).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_DECEPTION, character.getSkillProf(Character.Skill.Deception).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_HISTORY, character.getSkillProf(Character.Skill.History).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INSIGHT, character.getSkillProf(Character.Skill.Insight).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INTIMIDATION, character.getSkillProf(Character.Skill.Intimidation).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INVESTIGATION, character.getSkillProf(Character.Skill.Investigation).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_MEDICINE, character.getSkillProf(Character.Skill.Medicine).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_NATURE, character.getSkillProf(Character.Skill.Nature).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_PERCEPTION, character.getSkillProf(Character.Skill.Perception).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_PERFORMANCE, character.getSkillProf(Character.Skill.Performance).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_PERSUASION, character.getSkillProf(Character.Skill.Persuasion).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_RELIGION, character.getSkillProf(Character.Skill.Religion).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_SLEIGHT_OF_HAND, character.getSkillProf(Character.Skill.SleightOfHand).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_STEALTH, character.getSkillProf(Character.Skill.Stealth).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_SURVIVAL, character.getSkillProf(Character.Skill.Survival).ordinal());

        //Insert
        mDatabase.insert(CharacterContract.CharacterEntry.TABLE_NAME, null, values);
        mDatabase.close();
    }

    public void update(int id, Character character) {
        ContentValues values = new ContentValues();
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ID,character.getId());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_NAME, character.getName());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS, character.getCharacterClass());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_RACE, character.getRace());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_LEVEL, character.getLevel());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_STR, character.getStrScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_DEX, character.getDexScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CON, character.getConScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INT, character.getIntScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_WIS, character.getWisScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHA, character.getChaScore());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ACROBATICS, character.getSkillProf(Character.Skill.Acrobatics).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ANIMAL_HANDLING, character.getSkillProf(Character.Skill.AnimalHandling).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ARCANA, character.getSkillProf(Character.Skill.Arcana).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_ATHLETICS, character.getSkillProf(Character.Skill.Athletics).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_DECEPTION, character.getSkillProf(Character.Skill.Deception).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_HISTORY, character.getSkillProf(Character.Skill.History).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INSIGHT, character.getSkillProf(Character.Skill.Insight).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INTIMIDATION, character.getSkillProf(Character.Skill.Intimidation).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_INVESTIGATION, character.getSkillProf(Character.Skill.Investigation).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_MEDICINE, character.getSkillProf(Character.Skill.Medicine).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_NATURE, character.getSkillProf(Character.Skill.Nature).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_PERCEPTION, character.getSkillProf(Character.Skill.Perception).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_PERFORMANCE, character.getSkillProf(Character.Skill.Performance).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_PERSUASION, character.getSkillProf(Character.Skill.Persuasion).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_RELIGION, character.getSkillProf(Character.Skill.Religion).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_SLEIGHT_OF_HAND, character.getSkillProf(Character.Skill.SleightOfHand).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_STEALTH, character.getSkillProf(Character.Skill.Stealth).ordinal());
        values.put(CharacterContract.CharacterEntry.COLUMN_NAME_SURVIVAL, character.getSkillProf(Character.Skill.Survival).ordinal());

        //Update Row
        mDatabase.update(CharacterContract.CharacterEntry.TABLE_NAME, values, CharacterContract.CharacterEntry.COLUMN_NAME_ID + " = ?",
                new String[]{String.valueOf(id)});
        mDatabase.close();

    }

    public Cursor findAll() {
        Cursor cursor = mDatabase.query(CharacterContract.CharacterEntry.TABLE_NAME, CHARACTERS_ALL_COLUMNS, null, null, null, null, null);
        DatabaseUtils.dumpCursorToString(cursor);
        return cursor;
    }

    public Cursor findAllInfo() {
        return mDatabase.query(CharacterContract.CharacterEntry.TABLE_NAME, CHARACTERS_INFO_COLUMNS, null, null, null, null, null);

    }

    public void delete(int id){
    mDatabase.delete(CharacterContract.CharacterEntry.TABLE_NAME, CharacterContract.CharacterEntry.COLUMN_NAME_ID + "= ?", new String[]{Integer.toString(id)});
    }



}
