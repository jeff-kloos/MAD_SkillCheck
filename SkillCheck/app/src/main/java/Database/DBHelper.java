package Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import Model.DataObject.DataContract.CharacterContract;

/**
 * Created by Jeff on 23/03/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "character.db";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE =
            "CREATE TABLE " + CharacterContract.CharacterEntry.TABLE_NAME + " ("
                    + CharacterContract.CharacterEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "

                    + CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_NAME + " TEXT, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS + " TEXT, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_RACE + " TEXT, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_LEVEL + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_STR + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_DEX + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_CON + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_INT + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_WIS + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_CHA + " INTEGER, "

                    + CharacterContract.CharacterEntry.COLUMN_NAME_ACROBATICS + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_ANIMAL_HANDLING + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_ARCANA +            " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_ATHLETICS +       " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_DECEPTION +       " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_HISTORY +         " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_INSIGHT +         " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_INTIMIDATION +    " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_INVESTIGATION +   " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_MEDICINE +        " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_NATURE +          " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_PERCEPTION +      " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_PERFORMANCE +     " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_PERSUASION +      " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_RELIGION +        " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_SLEIGHT_OF_HAND + " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_STEALTH +         " INTEGER, "
                    + CharacterContract.CharacterEntry.COLUMN_NAME_SURVIVAL +      " INTEGER);";

    //Contructor
    public DBHelper(Context context) {
       super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CharacterContract.CharacterEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
