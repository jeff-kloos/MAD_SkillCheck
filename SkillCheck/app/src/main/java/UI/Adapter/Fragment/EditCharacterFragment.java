package UI.Adapter.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.jeff.skillcheck.R;

import java.util.ArrayList;
import java.util.List;

import Model.DataObject.Character;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditCharacterFragment.OnCharacterEditListener} interface
 * to handle interaction events.
 * Use the {@link EditCharacterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditCharacterFragment extends Fragment {

    private boolean isNewCharacter;
    private int index;

    private OnCharacterEditListener mListener;

    private EditText editLevel;
    private EditText editName;
    private EditText editClass;
    private EditText editRace;

    private EditText editSTR;
    private EditText editDEX;
    private EditText editCON;
    private EditText editINT;
    private EditText editWIS;
    private EditText editCHA;

    List<Spinner> skillSpinners = new ArrayList<Spinner>();

//   private Spinner spinnerAcrobatics;
//   private Spinner spinnerAnimalHandling;
//   private Spinner spinnerArcana;
//   private Spinner spinnerAthletics;
//   private Spinner spinnerDeception;
//   private Spinner spinnerHistory;
//   private Spinner spinnerInsight;
//   private Spinner spinnerIntimidation;
//   private Spinner spinnerInvestigation;
//   private Spinner spinnerMedicine;
//   private Spinner spinnerNature;
//   private Spinner spinnerPerception;
//   private Spinner spinnerPerformance;
//   private Spinner spinnerPersuasion;
//   private Spinner spinnerReligion;
//   private Spinner spinnerSleightOfHand;
//   private Spinner spinnerStealth;
//   private Spinner spinnerSurvival;

   private Button saveButton;

    private Character character;

    public EditCharacterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EditCharacterFragment.
     */
    public static EditCharacterFragment newInstance(Character character) {
        EditCharacterFragment fragment = new EditCharacterFragment();

        if (character == null) {
            fragment.character = new Character("NewCharacter", "Fighter", "Human");
            fragment.isNewCharacter = true;
        } else {
            fragment.character = character;
            fragment.isNewCharacter = false;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_character, container, false);

        saveButton = (Button) v.findViewById(R.id.button_save);

        editLevel = (EditText) v.findViewById(R.id.edit_level);
        editName = (EditText) v.findViewById(R.id.edit_name);
        editClass = (EditText) v.findViewById(R.id.edit_class);
        editRace = (EditText) v.findViewById(R.id.edit_Race);

        editSTR = (EditText) v.findViewById(R.id.edit_str);
        editDEX = (EditText) v.findViewById(R.id.edit_dex);
        editCON = (EditText) v.findViewById(R.id.edit_con);
        editINT = (EditText) v.findViewById(R.id.edit_int);
        editWIS = (EditText) v.findViewById(R.id.edit_wis);
        editCHA = (EditText) v.findViewById(R.id.edit_cha);

        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_acrobatics));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_animalHandling));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_arcana));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_athletics));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_deception));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_history));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_insight));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_intimidation));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_investigation));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_medicine));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_nature));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_perception));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_performance));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_persuasion));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_religion));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_slightOfHand));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_stealth));
        skillSpinners.add((Spinner) v.findViewById(R.id.spinner_survival));


        setupSpinners();


        setupViews(v);



        return v;
    }

    private void setupViews(View v) {
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed();
            }
        });

        editName.setText(character.getName());
        editClass.setText(character.getCharacterClass());
        editRace.setText(character.getRace());
        editLevel.setText(Integer.toString(character.getLevel()));

        if(isNewCharacter){


            editSTR.setText("10");
            editDEX.setText("10");
            editCON.setText("10");
            editINT.setText("10");
            editWIS.setText("10");
            editCHA.setText("10");
        } else {
            editSTR.setText(Integer.toString(character.getStrScore()));
            editDEX.setText(Integer.toString(character.getDexScore()));
            editCON.setText(Integer.toString(character.getConScore()));
            editINT.setText(Integer.toString(character.getIntScore()));
            editWIS.setText(Integer.toString(character.getWisScore()));
            editCHA.setText(Integer.toString(character.getChaScore()));
        }
    }

    private void setupSpinners() {
        index = -1;
        for(Spinner s : skillSpinners){
            index++;
            final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.array_skill_bonus, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            s.setAdapter(adapter);

            s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    character.setSkillProf(index, Character.SkillBonus.values()[i]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            if(!isNewCharacter){
                s.setSelection(character.getSkillProf(index).ordinal());
            }
        }
    }

    public void onButtonPressed() {
        if (mListener != null) {
            try{
                character.setLevel(Integer.parseInt(editLevel.getText().toString()));
                character.setName(editName.getText().toString());
                character.setCharacterClass(editClass.getText().toString());
                character.setRace(editRace.getText().toString());
                character.setStrScore(Integer.parseInt(editSTR.getText().toString()));
                character.setDexScore(Integer.parseInt(editDEX.getText().toString()));
                character.setConScore(Integer.parseInt(editCON.getText().toString()));
                character.setIntScore(Integer.parseInt(editINT.getText().toString()));
                character.setWisScore(Integer.parseInt(editWIS.getText().toString()));
                character.setChaScore(Integer.parseInt(editCHA.getText().toString()));
            } catch (Exception ex){
                ex.printStackTrace();
            }
            mListener.onSave(character, isNewCharacter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCharacterEditListener) {
            mListener = (OnCharacterEditListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCharcterEditListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnCharacterEditListener {
        void onSave(Character character, boolean isNew);
    }
}
