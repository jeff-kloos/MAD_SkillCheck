package UI.Adapter.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.jeff.skillcheck.R;

import Model.DataObject.Character;
import UI.Adapter.Fragment.EditCharacterFragment;
import Database.DataSource;

public class NewCharacterActivity extends AppCompatActivity implements EditCharacterFragment.OnCharacterEditListener {

    DataSource mDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_character);
        Fragment fragment = EditCharacterFragment.newInstance(null);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
        mDataSource = new DataSource(this);
    }

    @Override
    public void onSave(Character character, boolean isNew) {
        //TODO Add new Record to database.
            mDataSource.open();
            mDataSource.save(character);
        Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
        finish();
    }
}
