package UI.Adapter.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.jeff.skillcheck.R;

import UI.Adapter.CharacterInfoAdapter;
import Model.DataObject.Character;
import Database.DataSource;

public class CharacterListActivity extends AppCompatActivity implements CharacterInfoAdapter.OnItemSelectedListener {

    RecyclerView mRecyclerView;
    Cursor mCursor;
    DataSource mDataSource;
    CharacterInfoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.character_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDataSource = new DataSource(this);
        updateUI();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NewCharacterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        mDataSource.open();
        mCursor =  mDataSource.findAll();
        //mDataSource.close();

        if (mAdapter == null) {
            mAdapter = new CharacterInfoAdapter(mCursor);
            mAdapter.setOnItemSelectedListener(this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.swapCursor(mCursor);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_character_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, CharacterActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(Character character) {
        Intent intent = new Intent(this, CharacterActivity.class);

        intent.putExtra("character", character);

        intent.setAction(Intent.ACTION_EDIT);

        startActivity(intent);
    }
}
