package UI.Adapter.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeff.skillcheck.R;

import Model.DataObject.Character;
import Util.Roll;

public class CharacterActivity extends AppCompatActivity {



    private TabLayout mTabLayout;
    private Character mCharacter;

    private TextView mTextName;
    private TextView mTextDescription;
    private TextView getmTextName;

    private Button mButtonRoll;
    private Button mButtonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charcter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mCharacter = (Character) getIntent().getSerializableExtra("character");
        SetupViews();
    }

    private void SetupViews() {
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.STR));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.DEX));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.CON));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.INT));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.WIS));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.CHA));

        mTextName = (TextView) findViewById(R.id.text_character_name);
        mTextName.setText(mCharacter.getName());

        mTextDescription = (TextView) findViewById(R.id.character_description);
        mTextDescription.setText(mCharacter.getDescription());

        mButtonRoll = (Button) findViewById(R.id.button_roll);
        mButtonRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int roll = Roll.getInstance().d20();
                Toast.makeText(getBaseContext(), "Rolled: "  + (roll + mCharacter.getStatMod(getSelectedStat())), Toast.LENGTH_LONG).show();
                Log.d("Hello", "Roll: " + roll + " Stat = " + mCharacter.getStatMod(getSelectedStat()));
            }
        });


        Toast.makeText(this, mCharacter.getName(), Toast.LENGTH_SHORT).show();


    }

    private Character.Stat getSelectedStat(){
        int value = mTabLayout.getSelectedTabPosition();
        return Character.Stat.values()[value];
    }

}
