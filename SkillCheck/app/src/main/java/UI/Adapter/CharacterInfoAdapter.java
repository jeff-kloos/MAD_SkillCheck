package UI.Adapter;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jeff.skillcheck.R;

import Model.DataObject.DataContract.CharacterContract;
import Model.DataObject.Character;
/**
 * Created by Jeff on 23/03/2018.
 */

public class CharacterInfoAdapter extends RecyclerView.Adapter<CharacterInfoAdapter.CharacterInfoViewHolder> {

    private Cursor mCursor;
    private OnItemSelectedListener mListener;

    public CharacterInfoAdapter(Cursor cursor) {
        mCursor = cursor;
    }

    @Override
    public CharacterInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_character, parent, false);
        return new CharacterInfoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CharacterInfoViewHolder holder, int position) {
        //Move cursor
        mCursor.moveToPosition(position);

        final Character character = new Character();
        character.setId(mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_ID)));
        character.setName(mCursor.getString(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_NAME)));
        character.setLevel(mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_LEVEL)));
        character.setRace(mCursor.getString(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_RACE)));
        character.setCharacterClass(mCursor.getString(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS)));

        character.setStrScore (mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_STR)));
        character.setDexScore (mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_DEX)));
        character.setConScore (mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_CON)));
        character.setIntScore (mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_INT)));
        character.setWisScore (mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_WIS)));
        character.setChaScore (mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_CHA)));
        character.setSkillProf(Character.Skill.Acrobatics     , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_ACROBATICS))]);
        character.setSkillProf(Character.Skill.AnimalHandling , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_ANIMAL_HANDLING))]);
        character.setSkillProf(Character.Skill.Arcana         , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_ARCANA))]);
        character.setSkillProf(Character.Skill.Athletics      , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_ATHLETICS))]);
        character.setSkillProf(Character.Skill.Deception      , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_DECEPTION))]);
        character.setSkillProf(Character.Skill.History        , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_HISTORY))]);
        character.setSkillProf(Character.Skill.Insight        , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_INSIGHT))]);

        character.setSkillProf(Character.Skill.Intimidation   , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_INTIMIDATION))]);
        character.setSkillProf(Character.Skill.Investigation  , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_INVESTIGATION))]);
        character.setSkillProf(Character.Skill.Medicine       , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_MEDICINE))]);
        character.setSkillProf(Character.Skill.Nature         , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_NATURE))]);
        character.setSkillProf(Character.Skill.Perception     , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_PERCEPTION))]);
        character.setSkillProf(Character.Skill.Performance    , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_PERFORMANCE))]);
        character.setSkillProf(Character.Skill.Persuasion     , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_PERSUASION))]);
        character.setSkillProf(Character.Skill.Religion       , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_RELIGION))]);
        character.setSkillProf(Character.Skill.SleightOfHand  , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_SLEIGHT_OF_HAND))]);
        character.setSkillProf(Character.Skill.Stealth        , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_STEALTH))]);
        character.setSkillProf(Character.Skill.Survival       , Character.SkillBonus.values ()[mCursor.getInt(mCursor.getColumnIndex(CharacterContract.CharacterEntry.COLUMN_NAME_SURVIVAL))]);

        holder.bind(character);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemSelected(holder.getCharacter());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mCursor == null ? 0 : mCursor.getCount());
    }

    public void swapCursor(Cursor newCursor) {

        if (mCursor != null) mCursor.close();

        mCursor = newCursor;

        if (newCursor != null) {
            this.notifyDataSetChanged();
        }
    }

    public void setOnItemSelectedListener(OnItemSelectedListener mListener) {
        this.mListener = mListener;
    }

    class CharacterInfoViewHolder extends RecyclerView.ViewHolder {

        private Character character;
        private final TextView textName;
        private final TextView textDescription;

        CharacterInfoViewHolder(View itemView) {
            super(itemView);
            textName = (TextView) itemView.findViewById(R.id.character_name);
            textDescription = (TextView) itemView.findViewById(R.id.character_description);
        }

        public Character getCharacter(){
            return character;
        }

        void bind(final Character character) {
            this.character = character;
            textName.setText(character.getName());
            textDescription.setText(character.getDescription());
        }

    }

    public interface OnItemSelectedListener {
        void onItemSelected(Character character);
    }
}
