package UI.Adapter.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jeff.skillcheck.R;

public class SkillView extends RelativeLayout {

    TextView mTitle;
    TextView mModifier;


    public SkillView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SkillView,
                0, 0);

        setupViews();


    }

    private void setupViews() {
        inflate(getContext(), R.layout.view_skillview, this);
        mTitle = (TextView) findViewById(R.id.text_skill_title);
        mModifier = (TextView) findViewById(R.id.text_skill_mod);
    }

    public void setSkillMod(int value) {
        if(mModifier != null){
            mModifier.setText(Integer.toString(value));
        }
    }


}
