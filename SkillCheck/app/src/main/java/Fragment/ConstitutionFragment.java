package Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;

import CustomView.SkillView;
import DataObject.Character;


public class ConstitutionFragment extends Fragment {

    SkillView mRollView;
    SkillView mSaveView;

    Character character;


    public ConstitutionFragment() {
        // Required empty public constructor
    }


    public static ConstitutionFragment newInstance(Character character) {
        ConstitutionFragment frag = new ConstitutionFragment();
        Bundle args = new Bundle();
        args.putSerializable("char", character);
        frag.setArguments(args);
        return  frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            character = (Character) getArguments().getSerializable("char");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_constitution, container, false);

        mRollView = (SkillView) v.findViewById(R.id.skillview_con_roll);
        mRollView.setSkillMod(character.getStatMod(Character.Stat.Constitution));
        mSaveView = (SkillView) v.findViewById(R.id.skillview_con_save);
        mSaveView.setSkillMod(character.getStatMod(Character.Stat.Constitution));

        return  v;
    }

}
