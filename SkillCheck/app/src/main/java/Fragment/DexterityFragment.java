package Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;

import CustomView.SkillView;
import DataObject.Character;


public class DexterityFragment extends Fragment {

    SkillView mRollView;
    SkillView mSaveView;
    SkillView mAcrobaticsView;
    SkillView mSlightOfHandView;
    SkillView mStealthView;

    Character character;


    public DexterityFragment() {
        // Required empty public constructor
    }


    public static DexterityFragment newInstance(Character character) {
        DexterityFragment frag = new DexterityFragment();
        Bundle args = new Bundle();
        args.putSerializable("char", character);
        frag.setArguments(args);
        return  frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            character = (Character) getArguments().getSerializable("char");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dexterity, container, false);

        mRollView = (SkillView) v.findViewById(R.id.skillview_dex_roll);
        mRollView.setSkillMod(character.getStatMod(Character.Stat.Dexterity));
        mSaveView = (SkillView) v.findViewById(R.id.skillview_dex_save);
        mSaveView.setSkillMod(character.getStatMod(Character.Stat.Dexterity));

        mAcrobaticsView = (SkillView) v.findViewById(R.id.skillview_dex_acrobatics);
        mAcrobaticsView.setSkillMod(character.getSkillMod(Character.Skill.Acrobatics));

        mSlightOfHandView = (SkillView) v.findViewById(R.id.skillview_dex_sleightofhand);
        mSlightOfHandView.setSkillMod(character.getSkillMod(Character.Skill.SleightOfHand));

        mStealthView = (SkillView) v.findViewById(R.id.skillview_dex_stealth);
        mStealthView.setSkillMod(character.getSkillMod(Character.Skill.Stealth));

        return  v;
    }

}
