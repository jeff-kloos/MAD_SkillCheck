package Fragment;


import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;


public class SkillFragment extends Fragment {


    public SkillFragment() {
        // Required empty public constructor
    }


    public static SkillFragment newInstance() {
        return  new SkillFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_strength, container, false);
    }

    @LayoutRes
     public int getLayout(){
        return R.layout.fragment_strength;
    }

}
