package Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;

import CustomView.SkillView;
import DataObject.Character;


public class WisdomFragment extends Fragment {

    SkillView mRollView;
    SkillView mSaveView;
    SkillView mAnimalHandlingView;
    SkillView mInsightView;
    SkillView mMedicineView;
    SkillView mSurvivalView;

    Character character;


    public WisdomFragment() {
        // Required empty public constructor
    }


    public static WisdomFragment newInstance(Character character) {
        WisdomFragment frag = new WisdomFragment();
        Bundle args = new Bundle();
        args.putSerializable("char", character);
        frag.setArguments(args);
        return  frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            character = (Character) getArguments().getSerializable("char");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_wisdom, container, false);

        mRollView = (SkillView) v.findViewById(R.id.skillview_wis_roll);
        mRollView.setSkillMod(character.getStatMod(Character.Stat.Wisdom));
        mSaveView = (SkillView) v.findViewById(R.id.skillview_wis_save);
        mSaveView.setSkillMod(character.getStatMod(Character.Stat.Wisdom));

        mAnimalHandlingView = (SkillView) v.findViewById(R.id.skillview_wis_animal_handling);
        mAnimalHandlingView.setSkillMod(character.getSkillMod(Character.Skill.AnimalHandling));

        mInsightView = (SkillView) v.findViewById(R.id.skillview_wis_insight);
        mInsightView.setSkillMod(character.getSkillMod(Character.Skill.Insight));

        mMedicineView = (SkillView) v.findViewById(R.id.skillview_wis_medicine);
        mMedicineView.setSkillMod(character.getSkillMod(Character.Skill.Medicine));

        mSurvivalView = (SkillView) v.findViewById(R.id.skillview_wis_survival);
        mSurvivalView.setSkillMod(character.getSkillMod(Character.Skill.Survival));



        //mAthleticsView = (View.SkillView) v.findViewById(R.id.skillview_str_athletics);
        //mAthleticsView.setSkillMod(character.getSkillMod(Character.Skill.Athletics));

        return  v;
    }

}
