package Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;

import CustomView.SkillView;
import DataObject.Character;


public class IntelligenceFragment extends Fragment {

    SkillView mRollView;
    SkillView mSaveView;
    SkillView mArcanaView;
    SkillView mHistoryView;
    SkillView mInvestigationView;
    SkillView mNatureView;
    SkillView mReligionView;

    Character character;


    public IntelligenceFragment() {
        // Required empty public constructor
    }


    public static IntelligenceFragment newInstance(Character character) {
        IntelligenceFragment frag = new IntelligenceFragment();
        Bundle args = new Bundle();
        args.putSerializable("char", character);
        frag.setArguments(args);
        return  frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            character = (Character) getArguments().getSerializable("char");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_intelligence, container, false);

        mRollView = (SkillView) v.findViewById(R.id.skillview_int_roll);
        mRollView.setSkillMod(character.getStatMod(Character.Stat.Intelligence));
        mSaveView = (SkillView) v.findViewById(R.id.skillview_int_save);
        mSaveView.setSkillMod(character.getStatMod(Character.Stat.Intelligence));

        mArcanaView = (SkillView) v.findViewById(R.id.skillview_int_arcana);
        mArcanaView.setSkillMod(character.getSkillMod(Character.Skill.Arcana));

        mHistoryView = (SkillView) v.findViewById(R.id.skillview_int_history);
        mHistoryView.setSkillMod(character.getSkillMod(Character.Skill.History));

        mInvestigationView = (SkillView) v.findViewById(R.id.skillview_int_investigation);
        mInvestigationView.setSkillMod(character.getSkillMod(Character.Skill.Investigation));

        mNatureView = (SkillView) v.findViewById(R.id.skillview_int_nature);
        mNatureView.setSkillMod(character.getSkillMod(Character.Skill.Nature));

        mReligionView = (SkillView) v.findViewById(R.id.skillview_int_religion);
        mReligionView.setSkillMod(character.getSkillMod(Character.Skill.Religion));

        //mAthleticsView = (View.SkillView) v.findViewById(R.id.skillview_str_athletics);
        //mAthleticsView.setSkillMod(character.getSkillMod(Character.Skill.Athletics));

        return  v;
    }

}
