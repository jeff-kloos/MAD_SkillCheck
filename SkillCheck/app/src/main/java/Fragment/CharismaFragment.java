package Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;

import CustomView.SkillView;
import DataObject.Character;


public class CharismaFragment extends Fragment {

    SkillView mRollView;
    SkillView mSaveView;
    SkillView mDeceptionView;
    SkillView mIntimidationView;
    SkillView mPerformanceView;
    SkillView mPersuasionView;



    Character character;


    public CharismaFragment() {
        // Required empty public constructor
    }


    public static CharismaFragment newInstance(Character character) {
        CharismaFragment frag = new CharismaFragment();
        Bundle args = new Bundle();
        args.putSerializable("char", character);
        frag.setArguments(args);
        return  frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            character = (Character) getArguments().getSerializable("char");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_charisma, container, false);

        mRollView = (SkillView) v.findViewById(R.id.skillview_cha_roll);
        mRollView.setSkillMod(character.getStatMod(Character.Stat.Charisma));
        mSaveView = (SkillView) v.findViewById(R.id.skillview_cha_save);
        mSaveView.setSkillMod(character.getStatMod(Character.Stat.Charisma));

        mDeceptionView = (SkillView) v.findViewById(R.id.skillview_cha_deception);
        mDeceptionView.setSkillMod(character.getSkillMod(Character.Skill.Deception));

        mIntimidationView = (SkillView) v.findViewById(R.id.skillview_cha_intimidation);
        mIntimidationView.setSkillMod(character.getSkillMod(Character.Skill.Intimidation));

        mPerformanceView = (SkillView) v.findViewById(R.id.skillview_cha_performance);
        mPerformanceView.setSkillMod(character.getSkillMod(Character.Skill.Performance));

        mPersuasionView = (SkillView) v.findViewById(R.id.skillview_cha_persuasion);
        mPersuasionView.setSkillMod(character.getSkillMod(Character.Skill.Persuasion));






        //mAthleticsView = (View.SkillView) v.findViewById(R.id.skillview_str_athletics);
        //mAthleticsView.setSkillMod(character.getSkillMod(Character.Skill.Athletics));

        return  v;
    }

}
