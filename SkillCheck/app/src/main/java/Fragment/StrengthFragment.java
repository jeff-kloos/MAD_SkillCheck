package Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeff.skillcheck.R;

import CustomView.SkillView;
import DataObject.Character;


public class StrengthFragment extends Fragment {

    SkillView mRollView;
    SkillView mSaveView;
    SkillView mAthleticsView;

    Character character;


    public StrengthFragment() {
        // Required empty public constructor
    }


    public static StrengthFragment newInstance(Character character) {
        StrengthFragment frag = new StrengthFragment();
        Bundle args = new Bundle();
        args.putSerializable("char", character);
        frag.setArguments(args);
        return  frag;
}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            character = (Character) getArguments().getSerializable("char");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_strength, container, false);

        mRollView = (SkillView) v.findViewById(R.id.skillview_str_roll);
        mRollView.setSkillMod(character.getStatMod(Character.Stat.Strength));
        mSaveView = (SkillView) v.findViewById(R.id.skillview_str_save);
        mSaveView.setSkillMod(character.getStatMod(Character.Stat.Strength));
        mAthleticsView = (SkillView) v.findViewById(R.id.skillview_str_athletics);
        mAthleticsView.setSkillMod(character.getSkillMod(Character.Skill.Athletics));

        return  v;
    }

}
