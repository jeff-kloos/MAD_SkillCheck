package Model.DataObject;

/**
 * Created by Jeff on 15/03/2018.
 */

public class CharacterItemInfo {
    public CharacterItemInfo(String name, String description){

    }

    public CharacterItemInfo(){
    }

    private int id;
    private String name;
    private String description;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
