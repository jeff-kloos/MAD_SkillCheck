package Model.DataObject.DataContract;

import android.provider.BaseColumns;

/**
 * Created by Jeff on 23/03/2018.
 */

public final class CharacterContract {

    private CharacterContract(){}

    public static class CharacterEntry implements BaseColumns {
        public static final String TABLE_NAME = "Characters";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_CHARACTER_NAME = "Name";
        public static final String COLUMN_NAME_LEVEL = "Level";
        public static final String COLUMN_NAME_CHARACTER_CLASS = "Class";
        public static final String COLUMN_NAME_CHARACTER_RACE = "Race";
        public static final String COLUMN_NAME_STR = "Strength";
        public static final String COLUMN_NAME_DEX = "Dexterity";
        public static final String COLUMN_NAME_CON = "Constitution";
        public static final String COLUMN_NAME_INT = "Intelligence";
        public static final String COLUMN_NAME_WIS = "Wisdom";
        public static final String COLUMN_NAME_CHA = "Charisma";

        public static final String COLUMN_NAME_ACROBATICS =         "Acrobatics";
        public static final String COLUMN_NAME_ANIMAL_HANDLING =    "AnimalHandling";
        public static final String COLUMN_NAME_ARCANA =             "Arcana";
        public static final String COLUMN_NAME_ATHLETICS =          "Athletics";
        public static final String COLUMN_NAME_DECEPTION =          "Deception";
        public static final String COLUMN_NAME_HISTORY =            "History";
        public static final String COLUMN_NAME_INSIGHT =            "Insight";
        public static final String COLUMN_NAME_INTIMIDATION =       "Intimidation";
        public static final String COLUMN_NAME_INVESTIGATION =      "Investigation";
        public static final String COLUMN_NAME_MEDICINE =           "Medicine";
        public static final String COLUMN_NAME_NATURE =             "Nature";
        public static final String COLUMN_NAME_PERCEPTION =         "Perception";
        public static final String COLUMN_NAME_PERFORMANCE =        "Performance";
        public static final String COLUMN_NAME_PERSUASION =         "Persuasion";
        public static final String COLUMN_NAME_RELIGION =           "Religion";
        public static final String COLUMN_NAME_SLEIGHT_OF_HAND =    "SleightOfHand";
        public static final String COLUMN_NAME_STEALTH =            "Stealth";
        public static final String COLUMN_NAME_SURVIVAL =           "Survival";


    }
}
