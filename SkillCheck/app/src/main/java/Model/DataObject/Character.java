package Model.DataObject;

import java.io.Serializable;

/**
 * Created by Jeff on 20/03/2018.
 * This is a
 */

public class Character implements Serializable{

    public static class Skill {
            public static final int Acrobatics      = 0;
            public static final int AnimalHandling  = 1;
            public static final int Arcana          = 2;
            public static final int Athletics       = 3;
            public static final int Deception       = 4;
            public static final int History         = 5;
            public static final int Insight         = 6;
            public static final int Intimidation    = 7;
            public static final int Investigation   = 8;
            public static final int Medicine        = 9;
            public static final int Nature          = 10;
            public static final int Perception      = 11;
            public static final int Performance     = 12;
            public static final int Persuasion      = 13;
            public static final int Religion        = 14;
            public static final int SleightOfHand   = 15;
            public static final int Stealth         = 16;
            public static final int Survival        = 17;
    }

    public enum Stat {
        Strength,
        Dexterity,
        Constitution,
        Intelligence,
        Wisdom,
        Charisma
    }

    public enum SkillBonus {
        None,
        Proficiency,
        Expertise
    }

    public Character(){
        for (int i = 0; i <  skillBonuses.length; i++) {
            skillBonuses[i] = SkillBonus.None;
        }
    }

    public Character(String pName, String pClass, String pRace){
        this.level = 1;
        this.name = pName;
        this.characterClass = pClass;
        this.race = pRace;
        for (int i = 0; i <  skillBonuses.length; i++) {
            skillBonuses[i] = SkillBonus.None;
        }
    }


    private int id;
    private String name;
    private int level = 1;
    private String characterClass;
    private String race;

    private int strScore = 10;
    private int dexScore = 10;
    private int conScore = 10;
    private int intScore = 10;
    private int wisScore = 10;
    private int chaScore = 10;

    private SkillBonus[] skillBonuses = new SkillBonus[18];

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getStrScore() {
        return strScore;
    }

    public void setStrScore(int strScore) {
        this.strScore = strScore;
    }

    public int getDexScore() {
        return dexScore;
    }

    public void setDexScore(int dexScore) {
        this.dexScore = dexScore;
    }

    public int getConScore() {
        return conScore;
    }

    public void setConScore(int conScore) {
        this.conScore = conScore;
    }

    public int getIntScore() {
        return intScore;
    }

    public void setIntScore(int intScore) {
        this.intScore = intScore;
    }

    public int getWisScore() {
        return wisScore;
    }

    public void setWisScore(int wisScore) {
        this.wisScore = wisScore;
    }

    public int getChaScore() {
        return chaScore;
    }

    public void setChaScore(int chaScore) {
        this.chaScore = chaScore;
    }

    public void setSkillProf (int skill, SkillBonus bonus){
        skillBonuses[skill] = bonus;
    }

    public SkillBonus getSkillProf(int skill){
        return skillBonuses[skill];
    }

    public String getDescription(){
        String description = "Level " + level + " "
                + race + " " + characterClass;
        return description;
    }

    public int getStatMod(Stat stat){
        switch (stat){
            case Strength:
                return  (int) Math.floor((strScore -10)/2);
            case Dexterity:
                return  (int) Math.floor((dexScore -10)/2);
            case Constitution:
                return  (int) Math.floor((conScore -10)/2);
            case Intelligence:
                return  (int) Math.floor((intScore -10)/2);
            case Wisdom:
                return  (int) Math.floor((wisScore -10)/2);
            case Charisma:
                return  (int) Math.floor((chaScore -10)/2);
        }
        return 0;
    }

    public int getSkillMod(int skill) {
        int mod = 0;
        switch (skill){
            case -1:
                break;
            case Skill.Athletics:
                mod += getStatMod(Stat.Strength);
                break;
            case Skill.Acrobatics:
            case Skill.SleightOfHand:
            case Skill.Stealth:
                mod += getStatMod(Stat.Dexterity);
                break;
            case Skill.Arcana:
            case Skill.History:
            case Skill.Investigation:
            case Skill.Nature:
            case Skill.Religion:
                mod += getStatMod(Stat.Intelligence);
                break;
            case Skill.AnimalHandling:
            case Skill.Insight:
            case Skill.Medicine:
            case Skill.Perception:
            case Skill.Survival:
                mod += getStatMod(Stat.Wisdom);
                break;
            case Skill.Deception:
            case Skill.Intimidation:
            case Skill.Performance:
            case Skill.Persuasion:
                mod += getStatMod(Stat.Charisma);
                break;
        }

        switch (getSkillProf(skill)){
            case None:
                break;
            case Proficiency:
                mod += getProficiencyModifier();
                break;
            case Expertise:
                mod += getProficiencyModifier() *2;
                break;
        }

        return mod;

    }

    public int getProficiencyModifier(){
        if (level < 5){
            return 2;
        } else if (level < 9){
            return 3;
        } else if (level < 13){
            return 4;
        } else if (level < 17) {
            return 5;
        } else {
            return 6;
        }
    }

}
