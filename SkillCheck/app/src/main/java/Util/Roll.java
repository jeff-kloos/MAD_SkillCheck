package Util;

import java.util.Random;

/**
 * Created by Jeff on 26/03/2018.
 */

public class Roll {

    private static Roll instance;
    private static Random random;

    private Roll(){
        random = new Random();
    }

    public static Roll getInstance(){
        if (instance == null){
            instance = new Roll();
        }

        return  instance;
    }

    public int d20() {
        return random.nextInt(20) + 1;
    }

}
