package com.example.jeff.skillcheck.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.jeff.skillcheck.R;

import DataObject.Character;
import Fragment.EditCharacterFragment;
import Util.DataSource;

public class UpdateCharacterActivity extends AppCompatActivity implements EditCharacterFragment.OnCharacterEditListener {

    DataSource mDataSource;
    Character character;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_character);
        Bundle args = getIntent().getExtras();
        if (args != null){
            character = (Character) args.getSerializable("character");
        }

        Fragment fragment = EditCharacterFragment.newInstance(character);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
        mDataSource = new DataSource(this);
    }

    @Override
    public void onSave(Character character, boolean isNew) {
        mDataSource.open();
        mDataSource.update(character.getId(), character);
        finish();
    }
}
