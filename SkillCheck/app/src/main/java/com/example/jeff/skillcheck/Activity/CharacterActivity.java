package com.example.jeff.skillcheck.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeff.skillcheck.R;

import Adapter.StatsPageAdapter;
import DataObject.Character;
import Fragment.CharismaFragment;
import Fragment.ConstitutionFragment;
import Fragment.DexterityFragment;
import Fragment.IntelligenceFragment;
import Fragment.StrengthFragment;
import Fragment.WisdomFragment;
import Util.DataSource;

public class CharacterActivity extends AppCompatActivity implements View.OnClickListener {


    public static final int REQUEST_CODE_UPDATED = 12345;
    DataSource mDataSource;

    private StatsPageAdapter mPageAdapter;
    private ViewPager mViewPager;


    private TabLayout mTabLayout;
    private Character mCharacter;

    private TextView mTextName;
    private TextView mTextDescription;
    private TextView getmTextName;

    private Button mButtonDelete;
    private Button mButtonEdit;
    private Button mButtonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charcter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDataSource = new DataSource(this);

        mCharacter = (Character) getIntent().getSerializableExtra("character");
        SetupViews();
        setupViewPager();
    }

    private void setupViewPager() {
        mPageAdapter =  new StatsPageAdapter(getSupportFragmentManager());
        mPageAdapter.addFragment(StrengthFragment.newInstance(mCharacter), getString(R.string.STR));
        mPageAdapter.addFragment(DexterityFragment.newInstance(mCharacter), getString(R.string.DEX));
        mPageAdapter.addFragment(ConstitutionFragment.newInstance(mCharacter), getString(R.string.CON));
        mPageAdapter.addFragment(IntelligenceFragment.newInstance(mCharacter), getString(R.string.INT));
        mPageAdapter.addFragment(WisdomFragment.newInstance(mCharacter), getString(R.string.WIS));
        mPageAdapter.addFragment(CharismaFragment.newInstance(mCharacter), getString(R.string.CHA));
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mPageAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void SetupViews() {

        mTextName = (TextView) findViewById(R.id.text_character_name);
        mTextName.setText(mCharacter.getName());

        mTextDescription = (TextView) findViewById(R.id.character_description);
        mTextDescription.setText(mCharacter.getDescription());

        mButtonEdit = (Button) findViewById(R.id.button_edit);
        mButtonEdit.setOnClickListener(this);
        mButtonDelete = (Button) findViewById(R.id.button_delete);
        mButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog();
            }
        });


        Toast.makeText(this, mCharacter.getName(), Toast.LENGTH_SHORT).show();


    }

    private void createDialog() {
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setMessage(R.string.message_delete)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mDataSource.open();
                        mDataSource.delete(mCharacter.getId());
                        mDataSource.close();
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null).create().show();
    }

    private Character.Stat getSelectedStat(){
        int value = mTabLayout.getSelectedTabPosition();
        return Character.Stat.values()[value];
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getApplicationContext(), UpdateCharacterActivity.class);
        intent.putExtra("character", mCharacter);
        startActivityForResult(intent, REQUEST_CODE_UPDATED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_UPDATED) {
            finish();
        }
    }
}
