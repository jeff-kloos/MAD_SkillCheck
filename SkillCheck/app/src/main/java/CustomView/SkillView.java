package CustomView;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeff.skillcheck.R;

import Util.Roll;

public class SkillView extends RelativeLayout {

    private int modifier;

    TextView mTitle;
    TextView mModifier;


    public SkillView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SkillView,
                0, 0);

        setupViews(a);


    }

    private void setupViews(TypedArray a) {
        inflate(getContext(), R.layout.view_skillview, this);

        mTitle = (TextView) findViewById(R.id.text_skill_title);
        mTitle.setText(a.getText(R.styleable.SkillView_skillTitle));
        mModifier = (TextView) findViewById(R.id.text_skill_mod);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = Roll.getInstance().d20();
                Toast.makeText(getContext(), "Rolled: " + (result + modifier), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setSkillMod(int value) {
        modifier = value;
        if(mModifier != null){
            mModifier.setText("+ " + Integer.toString(value));
        }
    }


}